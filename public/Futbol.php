<?php
// Inicia la sesión
session_start();

// Verifica si la variable de sesión 'email' y 'password' están presentes
if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
    // La sesión está iniciada correctamente

    include('conexion/db.php');

    // Utilizar declaración preparada para prevenir la inyección SQL
    $stmt = $conexion->prepare("SELECT * FROM `usuario` WHERE email = ?");
    $stmt->bind_param("s", $_SESSION['email']);
    $stmt->execute();
    $resultado = $stmt->get_result();

    if ($resultado->num_rows == 1) {
        // Usuario encontrado, verificar la contraseña
        $usuario = $resultado->fetch_assoc();

        if (password_verify($_SESSION['password'], $usuario['password'])) {
            // Contraseña válida, mostrar el contenido
            echo 'Sesión iniciada correctamente. Bienvenido, ' . $_SESSION['email'];
        } else {
            // Contraseña incorrecta, redirigir al usuario a la página de inicio de sesión
            header("Location:login2.php");
            exit();
        }
    } else {
        // Usuario no encontrado, redirigir al usuario a la página de inicio de sesión
        header("Location:login2.php");
        exit();
    }
} else {
    // La sesión no está iniciada correctamente, redirige al usuario a la página de inicio de sesión
    header("Location:login2.php");
    exit();
}
?>
<!doctype html>
<html lang="es-ES">
<head>
<meta charset="utf-8">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title> Futbol Gratis</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
<link rel="stylesheet" href="styles.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
<meta name="google-site-verification" content="OF_KGAtKr-ZN82aA_DJxo7W_rKcHOHCENI41g22OTLE" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="Logo.png">
<style>
    .Acestream{
    font-size: 20px;
}
.Acestream a:hover{
	border-bottom: 2px solid #ffffff;
	
}
.Advertencia{
    font-size: 40px;
    text-align: center;
    padding: 20px;
}

</style>
	
</head>

<body>
    <div class="menu-btn">
        <i class="fas fa-bars"></i>
    </div>
    <div class="continer">
        <nav class="nav-main"> 
            <a href="index.html"><img src="imagenes/Logo.png" class="nav-brand" /></a>
            <ul class="nav-menu">  
           
                <li>
                    <a href="index.html">Inicio</a>
                </li>
                <li>
                    <a href="Articles.html">Artículos</a>
                </li>
                <li>
                    <a href="Futbol.php">Futbol</a>
                </li>
                <li>
                    <a href="Contacto.html">Contacto</a>
                </li>
            </ul>
            <ul class="nav-menu-right">
                <li>
                    <a href="conexion/logout.php"><i class="fas fa-user"></i></a>
                </li>
            </ul>
        </nav>
        <hr>
        <!--header class="Advertencia">
            <h1>Esta Página Web dejará de estar disponible para evitarnos posibles futuros Problemas Legales </h1>
        </header-->
		<div class="Tabla">
		    <ul class="Acestream">
		        <h4>Programas y apps con los enlaces</h4>
		        <a href="https://ouo.io/xEhLJr" rel="nofollow">Windows</a><br>
				<a href="https://ouo.io/zyL3EK" rel="nofollow">Android (Version 3.0)</a><br>
		        <a href="https://ouo.io/dvgKBP" rel="nofollow">Android (Version 2.0)</a><br>
				<a href="https://ouo.io/mAyPK5" rel="nofollow">Android versión (1.9.8)</a>
		    </ul>
		    <ul class="Acestream">
		        <h4>Programa Acestream (Requerido)</h4>
				<a href="https://download.acestream.media/Ace_Stream_Media_3.1.74.exe" rel="nofollow">Windows</a><br>
		        <a href="https://download.acestream.media/android/media.web/stable/AceStreamMedia-3.1.80.0-armv8_64.apk" rel="nofollow">Android ARMv8 (64 bits)</a><br>
		        <a href="https://download.acestream.media/android/media.web/stable/AceStreamMedia-3.1.80.0-armv7.apk" rel="nofollow">Android ARMv7 (32 bits)</a><br>
				<a href="https://download.acestream.media/products/acestream-engine/android/x86_64/latest" rel="nofollow">Android x86_64 (64 bits)</a><br>
		        <!--p>iOS (proximamente)</p-->
		    </ul>
			<ul class="LaLiga" >
			  <h4>LaLiga</h4>
				<a href="acestream://7d8c87e057be98f00f22e23b23fbf08999e4b02f" rel="nofollow">M. LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://7d8c87e057be98f00f22e23b23fbf08999e4b02f" rel="nofollow">M. LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://7d8c87e057be98f00f22e23b23fbf08999e4b02f" rel="nofollow">M.LaLiga 1080</a><br>
				<a href="acestream://1969c27658d4c8333ab2c0670802546121a774a5" rel="nofollow">M. LaLiga 720</a><br>
				<a href="acestream://fc2fe31b0bce25e2dc7ab4d262bf645e2be5a393" rel="nofollow">M. LaLiga 2 1080</a><br>
				<a href="acestream://91cf24543ac5a3dcd4a3557ce52de3f50ee6cb18" rel="nofollow">M. LaLiga 2 720</a><br>
				<a href="acestream://1960a9be8ae9e8c755330218eac4c5805466290a" rel="nofollow">DAZN LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://75251ba975132ec9a202806ba5bf606e87280c96" rel="nofollow">DAZN LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://a3bca895c58d3fc7d5e4259d3d5e3cf0291d1914" rel="nofollow">DAZN LaLiga 720</a><br>
				<a href="acestream://e33e666c393ef04ebe99a9b92135d2e0b48c4d10" rel="nofollow">DAZN LaLiga 2 1080 MultiAudio</a><br>
				<a href="acestream://0950c37fe1ea817fc561c611ae943d58505f7a79" rel="nofollow">DAZN LaLiga 2 720 MultiAudio</a><br>
				<a href="acestream://4c46585214b23b1d802ef2168060c7649a3894cf" rel="nofollow">LaLiga Hypermotion 1080</a><br>
				<a href="acestream://06b367c22394a1358c9cefa0cb5d0b64b9b2b3f4" rel="nofollow">LaLiga Hypermotion 720</a><br>
				<a href="acestream://d81b4f2f3fde433539c097b2edc9b587ca47b087" rel="nofollow">LaLiga Hypermotion 2 1080</a><br>
				<a href="acestream://d9cf1776ec4443e88aab3ca4b57647b00b4e439e" rel="nofollow">LaLiga Hypermotion 2 720</a><br>
				<a href="acestream://59ba45b79152e4599144177b234f6491fc19db53" rel="nofollow">LaLiga Hypermotion 3 1080</a><br>
				<!--a href="#" rel="nofollow">GOL TV 1080</a--><br>
			</ul>
			<ul class ="LaLiga">
				<h4>Copa del Rey</h4>
				<a href="acestream://3a4c8ac955d451bf3c29b45256e74aa0ea82d281" rel="nofollow">M.Copa del Rey 1080 MultiAudio</a><br>
				<a href="acestream://8ba764f6a3bce6eae87ec71208fad1aa3a20528d" rel="nofollow">M.Copa del Rey 1080 MultiAudio</a><br>
			</ul>
			<!--ul class="LaLiga">
				<h4>Final de LaLiga</h4>
				<a href="acestream://1e0214a06000e6f4847dd30cdc2fb839527be862" rel="nofollow">M. LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://d47addf5e467cbf102ba759600303c21efb1deeb" rel="nofollow">M. LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://5c668adaf86ed6af2488e28d9f3a4e3b0e909664" rel="nofollow">M. LaLiga 720</a><br>
				<a href="acestream://37d42d2b5d2278e6bb5810329a5f220867b3cf0c" rel="nofollow">M. LaLiga 2 1080</a><br>
				<a href="acestream://91cf24543ac5a3dcd4a3557ce52de3f50ee6cb18" rel="nofollow">M. LaLiga 2 720</a><br>
				<a href="acestream://7264961f63dc9dc6ef7acee7371f7a76b6a02061" rel="nofollow">DAZN LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://75251ba975132ec9a202806ba5bf606e87280c96" rel="nofollow">DAZN LaLiga 1080 MultiAudio</a><br>
				<a href="acestream://93d663e92a93845b11c9831885646fa39ba1ed66" rel="nofollow">DAZN LaLiga 2 1080 MultiAudio</a><br>
				<a href="acestream://8c71f0e0a5476e10950fc827f9d2a507340aba74" rel="nofollow">DAZN LaLiga 3 1080 MultiAudio</a><br>
				<a href="acestream://2792a8a5f4a3f53cd72dec377a2639cd12a6973e" rel="nofollow">DAZN LaLiga 4 1080 MultiAudio</a><br>
				<a href="acestream://ad372cba73aa0ece207a79532b3e30b731136bb2" rel="nofollow">M. LaLiga 3 1080</a><br>
				<a href="acestream://aa8f826da70e27a26b29c7b32402f17e8a67a8b0" rel="nofollow">M. LaLiga 4 1080</a><br>
				<a href="acestream://535394f62a810bc5aeb25be75ea5ff7d03e070b2" rel="nofollow">M. LaLiga 5 1080</a><br>
				<a href="acestream://c896d37778f9e43549a788fc22206a655895b51b" rel="nofollow">M. LaLiga 6 1080</a><br>
				<a href="acestream://1c0bb99925a0d18e075c2fe2cd7cb50946554ab1" rel="nofollow">M. LaLiga 7 1080</a><br>
			</ul-->
			<!--ul class="LaLiga">
				<h4>Final de LaLiga Smartbank</h4>
				<a href="acestream://7791ba828c0226d8f54cef1611889d29fb333247" rel="nofollow">LaLiga Smartbank 1080</a><br>
				<a href="acestream://ae84b2cc45ebf1148cc36ee5f801f26a04a2ac2c" rel="nofollow">LaLiga Smartbank 720</a><br>
				<a href="acestream://cf6249379184d88a61f9db617341c6b6e6abb342" rel="nofollow">LaLiga Smartbank 2 1080</a><br>
				<a href="acestream://d9cf1776ec4443e88aab3ca4b57647b00b4e439e" rel="nofollow">LaLiga Smartbank 2 720</a><br>
				<a href="acestream://59ba45b79152e4599144177b234f6491fc19db53" rel="nofollow">LaLiga Smartbank 3 1080</a><br>
				<a href="acestream://2cacf21476b036e319bcb7c7e747766e6ccc082e" rel="nofollow">LaLiga Smartbank 4</a><br>
				<a href="acestream://a1146358aa50c99c887108b17f62f9264186a16a" rel="nofollow">LaLiga Smartbank 5</a><br>
				<a href="acestream://7a9bb1b9cccb759c44ed84f3c1283922e6854670" rel="nofollow">LaLiga Smartbank 6 </a><br>
				<a href="acestream://446e73a22582921393b020ed08b768ad8e14d754" rel="nofollow">LaLiga Smartbank 7 </a><br>
				<a href="acestream://4d52fc1994fe927702aeb7bc8778e2f23b1260e2" rel="nofollow">LaLiga Smartbank 8</a><br>
				<a href="acestream://37d42d2b5d2278e6bb5810329a5f220867b3cf0c" rel="nofollow">M. LaLiga 2 1080</a><br>
				<a href="acestream://91cf24543ac5a3dcd4a3557ce52de3f50ee6cb18" rel="nofollow">M. LaLiga 2 720</a><br>
			</ul-->
			<ul class="Champions">
				<h4>UEFA Champions League</h4>
				<a href="acestream://931b1984badcb821df7b47a66ac0835ac871b51c" rel="nofollow">M.L. Campeones 1080 MultiAudio</a><br>
				<a href="acestream://f096a64dd756a6d549aa7b12ee9acf7eee27e833" rel="nofollow">M.L. Campeones 1080 MultiAudio</a><br>
 				<a href="acestream://e2e2aca792aae5da19995ac516b1d620531bd49c" rel="nofollow">M.L. Campeones 720</a><br>
 				<a href="acestream://fc2fe31b0bce25e2dc7ab4d262bf645e2be5a393" rel="nofollow">M.L. Campeones 2 1080</a><br>
				<a href="acestream://6753492c1908274c268a1b28e2a054a0ff8f86f9" rel="nofollow">M.L. Campeones 2 720</a><br>
				<a href="acestream://ad372cba73aa0ece207a79532b3e30b731136bb2" rel="nofollow">M.L. Campeones 3 1080</a><br>
				<a href="acestream://d59fe9978eed49f256b312a60671b5bce43d3f24" rel="nofollow">M.L. Campeones 3 720</a><br>
				<a href="acestream://1b48d5881337c87b89e1edefff6688fe2271d222" rel="nofollow">M.L. Campeones 4 1080</a><br>
				<a href="acestream://5c21c181156f200d5e628ef2c62cd06bf5f97eb7" rel="nofollow">M.L. Campeones 5 1080</a><br>
				<a href="acestream://a0978a100de9a307c12d395d9b2f4142db6ab52d" rel="nofollow">M.L. Campeones 6 1080</a><br>
				<a href="acestream://5932623d2fd7ed16b01787251b418e4f59a01cda" rel="nofollow">M.L. Campeones 7 1080</a><br>
				<a href="acestream://fdd9e3660059051660dbd7857443c2b807f258d0" rel="nofollow">M.L. Campeones 8 SD</a><br>
				<a href="acestream://69c95dc47b35dde559a6ef025c1b91c89646a17f" rel="nofollow">M.L. Campeones 9 SD</a><br>
				<a href="acestream://df67afe5c5f5e82a3ae629d126727b006b65e676" rel="nofollow">M.L. Campeones 10 SD</a><br>
				<a href="acestream://57e284a3c13100baeff7a88dc50949585c9f1277" rel="nofollow">M.L. Campeones 11 SD</a><br>
				<a href="acestream://33369549c635dabdb78b95c478c21fcc9e4ee854" rel="nofollow">M.L. Campeones 12 SD</a><br>
			</ul>
			<ul class="DAZN">
				<h4>Premier League + motor</h4>
				<a href="acestream://8ca07071b39185431f8e940ec98d1add9e561639" rel="nofollow">DAZN 1 1080</a><br>
				<a href="acestream://eaff9293c76a324c750ef5094c2a4e2c96518d1f" rel="nofollow">DAZN 1 720</a><br>
				<a href="acestream://60dbeeb299ec04bf02bc7426d827547599d3d9fc" rel="nofollow">DAZN 2 1080</a><br>
				<a href="acestream://7aa402bab9fff43258fbcf401881a39475f30aaf" rel="nofollow">DAZN 2 720</a><br>
				<a href="acestream://a8ffddef56f082d4bb5c0be0d3d2fdd8c16dbd97" rel="nofollow">DAZN 3 1080</a><br>
				<a href="acestream://2fcdf7a19c0858f686efdfabd3c8c2b92bf6bcfd" rel="nofollow">DAZN 4 1080</a><br>
				<a href="acestream://5789ca155323664edd293b848606688edf803f4d" rel="nofollow">DAZN F1 1080 (Fórmula 1)</a><br>
				<a href="acestream://9dad717d99b29a05672166258a77c25b57713dd5" rel="nofollow">DAZN F1 1080 (Fórmula 1)</a><br>
				<a href="acestream://e1fcad9de0c782c157fde6377805c58297ab65c2" rel="nofollow">DAZN F1 720 (Fórmula 1)</a><br>
			</ul>
			<ul class="Deportes">
				<h4>Deportes</h4>
				<a href="acestream://859bb6295b8d0f224224d3063d9db7cdeca03122" rel="nofollow">#Vamos 1080</a><br>
				<a href="acestream://3bba7c95857c2502c7e03ced1a6a9b00eb567fa0" rel="nofollow">#Vamos 720</a><br>
				<a href="acestream://67654e63b5065cdaa6c8e8d41bb5428b42b32830" rel="nofollow">#Ellas 1080</a><br>
				<a href="acestream://d00223931b1854163e24c5c22475015d7d45c112" rel="nofollow">M. Deportes 1080</a><br>
				<a href="acestream://e5fa927d9a017f4523fdb62774a0aec457985bfa" rel="nofollow">M. Deportes 720</a><br>
				<a href="acestream://e6f06d697f66a8fa606c4d61236c24b0d604d917" rel="nofollow">M. Deportes 2 1080</a><br>
				<a href="acestream://aee0a595220e0f1c2fee725fd1dbc602d7152a9a" rel="nofollow">M. Deportes 3 1080</a><br>
				<a href="acestream://42e83c337ece0af9ca7808859f84c7960e9cb6f5" rel="nofollow">M. Deportes 4 1080</a><br>
				<a href="acestream://b1e5abc48195b7ca9b2ee1b352e790eb9f7292e3" rel="nofollow">M. Deportes 5 1080</a><br>
				<a href="acestream://8587ed8ac36ac477e1d4176d3159a38bd154d4ce" rel="nofollow">M. Deportes 6 1080</a><br>
				<a href="acestream://2448f1d084f440eed2fbe847e24f1c02f5659a78" rel="nofollow">M. Deportes 7 1080</a><br>
			</ul>
			<ul class="DAZN">
				<h4>Futbol Femenino</h4>
				<a href="acestream://600222a4f98df80a2c0df2d60cb5ff3df9620710" rel="nofollow">DAZN Laliga F 1</a><br>
				<a href="acestream://d6cdd724a97fcf851e7ef641c28d6beb8663496e" rel="nofollow">DAZN Laliga F 2</a><br>
				<a href="acestream://162942adc047d0f78eac056effbe5bbec54a5e51" rel="nofollow">DAZN Laliga F 3</a><br>
				<a href="acestream://e454681a152a86da504e63694f17f90d0586867d" rel="nofollow">DAZN Laliga F 4</a><br>	
			</ul>
		</div>
	</div>
	<script src="main.js"></script>
</body>
</html>
