<?php

// Validar y obtener datos del formulario
$email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
$password = $_POST['password'];

if (!$email) {
    // Manejar el error de correo electrónico no válido
    header("Location: ./../login2.php?error=1");
    exit();
}

session_start();
$_SESSION['email'] = $email;
$_SESSION['password'] = $password;

include('db.php');

// Utilizar declaración preparada para prevenir la inyección SQL
$stmt = $conexion->prepare("SELECT * FROM `usuario` WHERE email = ?");
$stmt->bind_param("s", $email);
$stmt->execute();
$resultado = $stmt->get_result();

if ($resultado->num_rows == 1) {
    // Usuario encontrado, verificar la contraseña
    $usuario = $resultado->fetch_assoc();

    if (password_verify($password, $usuario['password'])) {
        // Contraseña válida, redirigir al usuario a la página principal
        header("Location: ./../Futbol.php");
        exit();
    }
}

// Usuario no válido o contraseña incorrecta, redirigir a la página de inicio de sesión con un código de error
header("Location: ./../login2.php?error=5");
exit();
?>
