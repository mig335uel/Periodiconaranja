<?php

session_start();

$newName = $_POST["new-name"];
$newEmail = $_POST["new-email"];
$newPassword = $_POST["new-password"];
$newPassword2 = $_POST["new-password2"];

include('db.php');

// Verificar si las contraseñas coinciden
if ($newPassword == $newPassword2) {

    // Hash y salt de la contraseña
    $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);

    // Validar que no exista un usuario con el mismo email
    $stmt = $conexion->prepare("SELECT * FROM `usuario` WHERE email = ?");
    $stmt->bind_param("s", $newEmail);
    $stmt->execute();
    $existingUserResult = $stmt->get_result();

    if ($existingUserResult->num_rows > 0) {
        header("Location: ./../login2.php?error=4");
        exit();
    }

    // Consulta para insertar el nuevo usuario en la base de datos (omitir el ID, asume que es autoincremental)
    $stmt = $conexion->prepare("INSERT INTO `usuario` (name, email, password) VALUES (?, ?, ?)");
    $stmt->bind_param("sss", $newName, $newEmail, $hashedPassword);
    $result = $stmt->execute();

    if (!$result) {
        $_SESSION['error'] = "Error al registrar el usuario. Por favor, inténtalo de nuevo.";
        header("Location: ./../login2.php?error=2");
        exit();
    } else {
       header("Location: ./../login2.php?succes=1");
        exit();
    }
}
?>