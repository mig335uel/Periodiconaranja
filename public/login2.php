<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Periódico Naranja</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
<link rel="stylesheet" href="styles.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
<meta name="google-site-verification" content="OF_KGAtKr-ZN82aA_DJxo7W_rKcHOHCENI41g22OTLE" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="Logo.png">
<style>
</style>

</head>

<body class="showcase-color">
    <div class="menu-btn">
        <i class="fas fa-bars"></i>
    </div>
    <div class="continer">
        <nav class="nav-main"> 
            <img src="imagenes/Logo.png" class="nav-brand" />
            <ul class="nav-menu">  
           
                <li>
                    <a href="index.html">Inicio</a>
                </li>
                <li>
                    <a href="Articles.html">Artículos</a>
                </li>
                <li>
                    <a href="Futbol.php">Futbol</a>
                </li>
                <li>
                    <a href="Contacto.html">Contacto</a>
                </li>
            </ul>
            <ul class="nav-menu-right">
                <li>
                    <a href="#">
                        <i class="fas fa-search"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <hr>
        
       <div class="login-continer">
    
            <form action="conexion/validar.php" method="post" id="login">
                <input type="text" placeholder="email" name="email" id="email">
                <input type="password" placeholder="Contraseña" name="password" id="password">
                <p class="message">¿No tienes una cuenta? <a  id="cambio">Registrate</a></p>
                <br>
                <button class="joinbtn">Iniciar Sesion</button>
                <?php
                if(isset($_GET['error']) && $_GET['error'] == 1){
                    echo"
                            <p class='error'>Error en la autenticacion</p>
                            
                            ";
                }
                if (isset($_GET['error']) && $_GET['error'] == 2) {
                    echo "
                            <p class='error'>Error al registrar el usuario. Por favor, inténtalo de nuevo.</p>
                            
                            ";
                }
                if (isset($_GET['succes']) && $_GET['succes'] == 1) {
                    echo "
                            <p class='succes'>¡Registro exitoso! Ahora puedes iniciar sesión.</p>
                            
                            ";
                }
                if(isset($_GET['error']) && $_GET['error'] == 3){
                    echo"
                        <p>Las contraseñas no coinciden.</p>
                    ";
                }
                if(isset($_GET['error']) && $_GET['error'] == 4){
                    echo"
                        <p>Ya existe un usuario con ese correo electrónico.</p>
                    ";
                }
                
                if(isset($_GET['error']) && $_GET['error'] == 5){
                    echo"
                        <p>El usuario o la contraseña no son correctos</p>
                    ";
                }
                ?>

            </form>
           <form id="registro" action="conexion/registro.php" method="post", style="display: none">
                <input type="text" placeholder="Nombre" name="new-name" id="new-name">
                <input type="text" placeholder="email" name="new-email" id="new-email">
                <input type="password" placeholder="Contraseña" name="new-password" id="new-password">
                <input type="password" placeholder="Confirmar Contraseña" name="new-password2" id="new-password2">
                <p>ya tienes una cuenta? <a id="cambio2">Inicia Sesion</a></p>
               <br>
               <button class="joinbtn">Registrarse</button>


            </form>
        
        
        
       </div>
    </div>
    <script>
        const cambio = document.getElementById('cambio');
        const cambio2 = document.getElementById('cambio2');

        cambio.addEventListener('click', () => {
            document.getElementById('login').style.display = 'none';
            document.getElementById('registro').style.display = 'block';
        });
        cambio2.addEventListener('click', () => {
            document.getElementById('login').style.display = 'block';
            document.getElementById('registro').style.display = 'none';
        });
    </script>
    <script src="main.js"></script>
    <script src="https://unpkg.com/scrollreveal"></script>
</body>
</html>